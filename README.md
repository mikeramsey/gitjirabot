# GitJiraBot

[![pipeline status](https://gitlab.com/mikeramsey/gitjirabot/badges/main/pipeline.svg)](https://gitlab.com/mikeramsey/gitjirabot/pipelines)
[![coverage report](https://gitlab.com/mikeramsey/gitjirabot/badges/main/coverage.svg)](https://gitlab.com/mikeramsey/gitjirabot/commits/master)
[![documentation](https://img.shields.io/badge/docs-mkdocs%20material-blue.svg?style=flat)](https://mikeramsey.gitlab.io/gitjirabot/)
[![pypi version](https://img.shields.io/pypi/v/gitjirabot.svg)](https://pypi.org/project/gitjirabot/)
[![gitter](https://badges.gitter.im/join%20chat.svg)](https://gitter.im/gitjirabot/community)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Git Jira integrations bot for automating complex tasks

## Requirements

GitJiraBot requires Python 3.6 or above.

<details>
<summary>To install Python 3.6, I recommend using <a href="https://github.com/pyenv/pyenv"><code>pyenv</code></a>.</summary>

```bash
# install pyenv
git clone https://github.com/pyenv/pyenv ~/.pyenv

# setup pyenv (you should also put these three lines in .bashrc or similar)
export PATH="${HOME}/.pyenv/bin:${PATH}"
export PYENV_ROOT="${HOME}/.pyenv"
eval "$(pyenv init -)"

# install Python 3.6
pyenv install 3.6.12

# make it available globally
pyenv global system 3.6.12
```
</details>

## Installation

With `pip`:
```bash
python3.6 -m pip install gitjirabot
```

With [`pipx`](https://github.com/pipxproject/pipx):
```bash
python3.6 -m pip install --user pipx

pipx install --python python3.6 gitjirabot
```
