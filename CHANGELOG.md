# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

<!-- insertion marker -->
## [0.1.6](https://gitlab.com/mikeramsey/gitjirabot/tags/0.1.6) - 2021-11-14

<small>[Compare with 0.1.5](https://gitlab.com/mikeramsey/gitjirabot/compare/0.1.5...0.1.6)</small>

### Bug Fixes
- Fix mkdocs.yml nav ([17fc386](https://gitlab.com/mikeramsey/gitjirabot/commit/17fc386ad6eb83a14d0381223356597d68285a7f) by Michael Ramsey).


## [0.1.5](https://gitlab.com/mikeramsey/gitjirabot/tags/0.1.5) - 2021-11-14

<small>[Compare with 0.1.4](https://gitlab.com/mikeramsey/gitjirabot/compare/0.1.4...0.1.5)</small>

### Bug Fixes
- Fix mypy macros.py for mkdocs some python versions ([ef5c278](https://gitlab.com/mikeramsey/gitjirabot/commit/ef5c278bf001d9c73a5507a607bdb463869c5a72) by Michael Ramsey).


## [0.1.4](https://gitlab.com/mikeramsey/gitjirabot/tags/0.1.4) - 2021-11-14

<small>[Compare with 0.1.3](https://gitlab.com/mikeramsey/gitjirabot/compare/0.1.3...0.1.4)</small>

### Bug Fixes
- Fix gitlab-ci.yml badge links to main and also for CI deploy pages ([21f83b0](https://gitlab.com/mikeramsey/gitjirabot/commit/21f83b0cc57fb53fb581b892d388f78e050e8407) by Michael Ramsey).

## [0.1.3](https://gitlab.com/mikeramsey/gitjirabot/tags/0.1.3) - 2021-11-13

<small>[Compare with 0.1.2](https://gitlab.com/mikeramsey/gitjirabot/compare/0.1.2...0.1.3)</small>


## [0.1.2](https://gitlab.com/mikeramsey/gitjirabot/tags/0.1.2) - 2021-11-13

<small>[Compare with 0.1.1](https://gitlab.com/mikeramsey/gitjirabot/compare/0.1.1...0.1.2)</small>

### Bug Fixes
- Fix config and python version in gitlab-ci.yml for CI and pages ([a81783f](https://gitlab.com/mikeramsey/gitjirabot/commit/a81783f4555dfc8fe974ed1134c4c321b063e1cb) by Michael Ramsey).


## [0.1.1](https://gitlab.com/mikeramsey/gitjirabot/tags/0.1.1) - 2021-11-13

<small>[Compare with 0.1.0](https://gitlab.com/mikeramsey/gitjirabot/compare/0.1.0...0.1.1)</small>

### Bug Fixes
- Fix mypy for docs/macros.py for CI checks ([3d5edac](https://gitlab.com/mikeramsey/gitjirabot/commit/3d5edaccc2948cdde12a5f9e4602fa072af2da3a) by Michael Ramsey).




## [0.1.0](https://gitlab.com/mikeramsey/gitjirabot/tags/0.1.0) - 2021-11-13

<small>[Compare with first commit](https://gitlab.com/mikeramsey/gitjirabot/compare/7cd48781bb2507858709749947ebabb0ebe9c750...0.1.0)</small>

### Code Refactoring
- Add poetry.lock file and dependicies ([33832bc](https://gitlab.com/mikeramsey/gitjirabot/commit/33832bcc9a399197af03c5ac8dd593f3c113a840) by Michael Ramsey).
