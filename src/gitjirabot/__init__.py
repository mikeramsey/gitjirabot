"""
GitJiraBot package.

Git Jira integrations bot for automating complex tasks
"""

from typing import List

__all__: List[str] = []  # noqa: WPS410 (the only __variable__ we use)
